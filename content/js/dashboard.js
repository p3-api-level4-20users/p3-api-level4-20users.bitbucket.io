/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 81.4385150812065, "KoPercent": 18.561484918793504};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.16241299303944315, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.3333333333333333, 500, 1500, "getChildHealthRecords"], "isController": false}, {"data": [0.6595744680851063, 500, 1500, "getClassAttendanceSummaries"], "isController": false}, {"data": [0.0, 500, 1500, "getChildStatementOfAccount"], "isController": false}, {"data": [0.08695652173913043, 500, 1500, "childByID"], "isController": false}, {"data": [0.0, 500, 1500, "findClassActivities"], "isController": false}, {"data": [0.05, 500, 1500, "getAllClassInfo"], "isController": false}, {"data": [0.0, 500, 1500, "getChildrenToAssignToClass"], "isController": false}, {"data": [0.40217391304347827, 500, 1500, "bankAccountsByFkChild"], "isController": false}, {"data": [0.0, 500, 1500, "getChildrenData"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 431, 80, 18.561484918793504, 13012.696055684442, 7, 70947, 9003.0, 33198.40000000001, 42339.999999999956, 62389.120000000024, 1.3718512671322263, 1.7720916345321385, 4.835456800013687], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["getChildHealthRecords", 42, 10, 23.80952380952381, 3875.857142857143, 43, 15569, 2178.0, 9011.3, 9037.7, 15569.0, 0.1619520621895919, 0.07980868689653577, 0.3908042438188296], "isController": false}, {"data": ["getClassAttendanceSummaries", 47, 6, 12.76595744680851, 2577.6595744680853, 7, 18415, 401.0, 9003.6, 9033.2, 18415.0, 0.15925509277456254, 0.09344258155046692, 0.169519581176048], "isController": false}, {"data": ["getChildStatementOfAccount", 49, 12, 24.489795918367346, 37111.44897959184, 2796, 70947, 36783.0, 59867.0, 65857.0, 70947.0, 0.15701253216353656, 0.18286001932215448, 0.25591202752045167], "isController": false}, {"data": ["childByID", 46, 9, 19.565217391304348, 8139.847826086956, 406, 23579, 9003.0, 19054.100000000002, 20663.35, 23579.0, 0.16601403901329917, 0.3511832729577567, 2.113436535720086], "isController": false}, {"data": ["findClassActivities", 53, 10, 18.867924528301888, 23091.018867924526, 9002, 46712, 22285.0, 39300.200000000004, 44007.7, 46712.0, 0.17307704867710355, 0.15131485490918353, 0.5672329837503511], "isController": false}, {"data": ["getAllClassInfo", 50, 7, 14.0, 15852.38, 68, 35152, 17652.5, 28240.399999999998, 32625.299999999992, 35152.0, 0.16322852973534124, 0.39624044398976227, 0.4190701217521603], "isController": false}, {"data": ["getChildrenToAssignToClass", 50, 14, 28.0, 14793.560000000001, 2027, 40472, 11641.0, 28934.599999999995, 38027.299999999996, 40472.0, 0.1611058304200029, 0.4240922189911553, 0.6404900738670232], "isController": false}, {"data": ["bankAccountsByFkChild", 46, 7, 15.217391304347826, 2573.260869565217, 18, 9031, 1449.5, 9003.3, 9018.3, 9031.0, 0.18347233356865653, 0.10615563389691249, 0.23525309958559185], "isController": false}, {"data": ["getChildrenData", 48, 5, 10.416666666666666, 5357.312499999999, 1530, 17155, 3520.5, 10145.300000000005, 14335.15, 17155.0, 0.16523065166280554, 0.09690223724023504, 0.49052849712395397], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["502/Bad Gateway", 80, 100.0, 18.561484918793504], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 431, 80, "502/Bad Gateway", 80, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": ["getChildHealthRecords", 42, 10, "502/Bad Gateway", 10, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getClassAttendanceSummaries", 47, 6, "502/Bad Gateway", 6, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getChildStatementOfAccount", 49, 12, "502/Bad Gateway", 12, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["childByID", 46, 9, "502/Bad Gateway", 9, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["findClassActivities", 53, 10, "502/Bad Gateway", 10, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getAllClassInfo", 50, 7, "502/Bad Gateway", 7, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getChildrenToAssignToClass", 50, 14, "502/Bad Gateway", 14, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["bankAccountsByFkChild", 46, 7, "502/Bad Gateway", 7, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getChildrenData", 48, 5, "502/Bad Gateway", 5, null, null, null, null, null, null, null, null], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
